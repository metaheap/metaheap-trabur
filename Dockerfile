FROM node:10

# Create app directory
RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

# Bundle APP files
COPY package.json /usr/src/app/

# Install app dependencies
RUN npm install --silent

# Install pm2 so we can run our application
RUN npm i -g pm2

# Bundle app source
COPY . /usr/src/app

RUN npm run build

EXPOSE 7548

# CMD [ "npm", "start" ]
# CMD [ "pm2", "start", "pm2.config.js", "--no-daemon", "--env=production" ]

# ENV PM2_PUBLIC_KEY w5cltz0ludpodtm
# ENV PM2_SECRET_KEY rbbh5g19et2kd5s

CMD [ "pm2-runtime", "pm2.config.js", "--env=production" ]

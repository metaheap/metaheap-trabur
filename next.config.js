const isProd = process.env.NODE_ENV === 'production'

module.exports = {
  serverRuntimeConfig: { // Will only be available on the server side
    // mySecret: 'secret',
    // secondSecret: process.env.SECOND_SECRET // Pass through env variables
  },
  publicRuntimeConfig: { // Will be available on both server and client
    // staticFolder: '/static',
    // You may only need to add assetPrefix in the production.
    assetPrefix: isProd ? process.env.ASSET_PREFIX : 'https://metaheap.sfo2.cdn.digitaloceanspaces.com/trabur'
  }
}
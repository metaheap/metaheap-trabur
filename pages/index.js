import React from 'react'
import Link from 'next/link'
import IndexLayout from '../components/IndexLayout'

import {
  Container
} from 'semantic-ui-react'

class Index extends React.Component {
  static async getInitialProps (context, apolloClient) {
    return {}
  }

  constructor(props) {
    super(props);
    
    this.state = {}
  }

  render () {
    return (
      <IndexLayout 
        title='TRAVIS BURANDT'
        meta='Welcome to trabur.metaheap.io! ~Travis'
        activeItem='home'
      >
        <Container
          style={{
            background: 'rgba(255,255,255, 0.3)',
            border: '2px solid #fff',
            padding: '1em',
            color: '#fff',
            fontSize: '2em',
            lineHeight: '1.5em'
          }}
        >
          <div>Welcome to <a href='/'>trabur.metaheap.io</a>!</div>
          <div>Hello, my name is <strong>Travis Burandt</strong> and I am a <strong>JavaScript</strong> developer with 10+ years of experience building software. Dating back to the year 2008 I started learning <strong>HTML</strong> and <strong>CSS</strong> with <a href='//www.halotracks.org' target='_blank'>my first website</a> when I wanted to customize it beyond using the built in buttons and dials. The website is still alive and kicking to this day!</div>
          <br />
          <div>Many people know me as a cyclist more than a computer programmer. Others know me online as a video gamer. I consider myself a Full-Stack JavaScript Developer, and I am equally comfortable developing <strong>SQL</strong> and <strong>GraphQL</strong>. My technical expertise also includes <strong>react</strong>, <strong>react-native</strong>, a bit of <strong>node.js</strong>, and the surrounding libraries/frameworks. I like deploying to the cloud using containerization for production and deploying bare-metal for dev/test. Checkout <a href='/blog/my-lab-setup-for-full-stack-javascript-development'>My Lab Setup for Full Stack JavaScript Development</a> for details on just how I achieve that.</div>
          <br />
          <div>My first progamming language was <strong>PHP</strong> when I built an early version of <a href='//www.fleetgrid.com' target='_blank'>fleetgrid</a>. Simultaneously, I was also going to college for a computer science degree taking courses on <strong>Java</strong>. I found out quickly that college is not for me and by chance landed a great job in the <a href='https://www.linkedin.com/company/gritness/' target='_blank'>fitness</a> industry. I learned a lot, refocused on my goals, and thats when I became an <strong>angular.js</strong> front-end developer dabbling with jobs in the <a href='https://www.docbookmd.com/' target='_blank'>health care</a> industry, and the <a href='http://www.pnimedia.com/' target='_blank'>printing</a> industry. Since then I've come full circle back to working on fleetgrid and others but with JavaScript as my primary language of choice.</div>
          <br />
          <div>In my <Link href='/portfolio'><a>portflio</a></Link> you will find projects that I currently maintain or recently worked on. In my <Link href='/blog'><a>blog</a></Link> you will find topics pertaining to subjects that I encounter from day to day software development.</div>
          <br />
          <div>~Travis</div>
        </Container>
      </IndexLayout>
    )
  }
}

export default Index
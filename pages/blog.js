import React from 'react'
import Link from 'next/link'
import IndexLayout from '../components/IndexLayout'

import {
  Container,
  Image,
  Card,
  Breadcrumb,
  Grid,
  Button,
  Icon
} from 'semantic-ui-react'

import fetch from 'isomorphic-unfetch'
import moment from 'moment'
import TagCloud from '../components/TagCloud'
import matchSorter from 'match-sorter'

import getConfig from 'next/config'
const {serverRuntimeConfig, publicRuntimeConfig} = getConfig()

class Blog extends React.Component {
  static async getInitialProps (context, apolloClient) {
		const blogs = await fetch(`${publicRuntimeConfig.assetPrefix}/output/blog.json`)
			.then((response) => {
        return response.json()
      })
			.then((data) => {
        let blogs = []
        Object.keys(data.fileMap).map(function (value, index) {
          blogs.push(data.fileMap[value])
        })

        return blogs
		  })
			.catch((error) => {
        console.error(error.toString())
      })

    const search = context.query.search

    return { 
      blogs: matchSorter(
        blogs,
        search,
        {
          keys: [
            'title',
            'description',
            'tags',
            'bodyContent'
          ]
        }
      ), 
      search 
    }
  }

  constructor(props) {
    super(props);
    
    this.state = {
      data: this.props.blogs,
      search: this.props.search
    }
  }

  render () {
    return (
      <IndexLayout 
        title='Blog - TRAVIS BURANDT'
        meta=''
        activeItem='blog'
        search={this.state.search}
      >
        <Container
          style={{
            background: 'rgba(0, 0, 0, 0.8)',
            border: '2px solid #fff',
            padding: '1em',
            marginBottom: '1em',
            color: '#fff'
          }}
        >
          <Grid>
            <Grid.Row>
              <Grid.Column width={8}>
                <Breadcrumb size='massive'>
                  <Link href={'/'}>
                    <Breadcrumb.Section link style={{ color: '#f7bf4d' }}>TRABUR</Breadcrumb.Section>
                  </Link>
                  <Breadcrumb.Divider icon='right chevron' style={{ color: '#ccc' }} />
                  <Breadcrumb.Section>Blog</Breadcrumb.Section>
                </Breadcrumb>
              </Grid.Column>
              <Grid.Column width={8} style={{ textAlign: 'right' }}>
                {
                  this.state.search
                    ?
                      <Breadcrumb size='massive'>
                        <Breadcrumb.Section style={{ color: '#ccc', marginRight: '2em' }}>
                          Showing search results for "<a href={`/blog?search=${this.state.search}`}>{this.state.search}</a>"
                        </Breadcrumb.Section>
                        <a href={'/blog'}>
                          <Button icon size='mini' style={{ 
                            position: 'absolute',
                            right: '1em',
                            top: '-0.2em'
                          }}>
                            <Icon name='close' />
                          </Button>
                        </a>
                      </Breadcrumb>
                    : null
                }
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
        <TagCloud array={this.state.data} type='blog' search={this.state.search} />
        <Container>
          <Card.Group centered>
            {
              this.state.data.map(function (value, index) {
                return (
                  <Link key={`${value.dir}/${value.base}`} href={`/blog/${value.slug}`}>
                    <Card>
                      <Image  src={`${publicRuntimeConfig.assetPrefix}${value.coverPreview}`} />
                      <Card.Content>
                        <Card.Header>{value.title}</Card.Header>
                        <Card.Meta>Created: {moment(value.createdAt).fromNow()}</Card.Meta>
                        <Card.Description>{value.description}</Card.Description>
                      </Card.Content>
                    </Card>
                  </Link>
                )
              })
            }
          </Card.Group>
        </Container>
      </IndexLayout>
    )
  }
}

export default Blog
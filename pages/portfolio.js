import React from 'react'
import Link from 'next/link'
import IndexLayout from '../components/IndexLayout'

import {
  Container,
  Breadcrumb,
  Grid,
  Button,
  Icon
} from 'semantic-ui-react'

import Showcase from '../components/Showcase'
import TagCloud from '../components/TagCloud'
import matchSorter from 'match-sorter'
import fetch from 'isomorphic-unfetch'

import getConfig from 'next/config'
const {serverRuntimeConfig, publicRuntimeConfig} = getConfig()

class Portfolio extends React.Component {
  static async getInitialProps (context, apolloClient) {
		const projects = await fetch(`${publicRuntimeConfig.assetPrefix}/output/portfolio.json`)
      .then((response) => {
        return response.json()
      })
      .then((data) => {
        let projects = []
        Object.keys(data.fileMap).map(function (value, index) {
          projects.push(data.fileMap[value])
        })

        return projects
      })
      .catch((error) => {
        console.error(error.toString())
      })

    const type = context.query.type
    const search = context.query.search

    return { 
      projects: matchSorter(
        projects,
        search,
        {
          keys: [
            'tags'
          ]
        }
      ), 
      type, 
      search 
    }
  }

  constructor(props) {
    super(props);
    
    this.state = {
      projects: this.props.projects,
      type: this.props.type,
      search: this.props.search
    }
  }

  render () {
    return (
      <IndexLayout 
        title='Portfolio - TRAVIS BURANDT'
        meta=''
        activeItem='portfolio'
        search={this.state.search}
      >
        <Container
          style={{
            background: 'rgba(0, 0, 0, 0.8)',
            border: '2px solid #fff',
            padding: '1em',
            marginBottom: '1em',
            color: '#fff'
          }}
        >
          <Grid>
            <Grid.Row>
              <Grid.Column width={8}>
                <Breadcrumb size='massive'>
                  <Link href={'/'}>
                    <Breadcrumb.Section link style={{ color: '#f7bf4d' }}>TRABUR</Breadcrumb.Section>
                  </Link>
                  <Breadcrumb.Divider icon='right chevron' style={{ color: '#ccc' }} />
                  <Breadcrumb.Section>Portfolio</Breadcrumb.Section>
                </Breadcrumb>
              </Grid.Column>
              <Grid.Column width={8} style={{ textAlign: 'right' }}>
                {
                  this.state.search
                    ?
                      <Breadcrumb size='massive'>
                        <Breadcrumb.Section style={{ color: '#ccc', marginRight: '2em' }}>
                          Showing search results for "<a href={`/portfolio?search=${this.state.search}`}>{this.state.search}</a>"
                        </Breadcrumb.Section>
                        <a href={'/portfolio'}>
                          <Button icon size='mini' style={{ 
                            position: 'absolute',
                            right: '1em',
                            top: '-0.2em'
                          }}>
                            <Icon name='close' />
                          </Button>
                        </a>
                      </Breadcrumb>
                    : null
                }
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
        <TagCloud array={this.state.projects} type='portfolio' search={this.state.search} />
        <Container>
          <Showcase projects={this.state.projects} type={this.state.type} />
        </Container>
      </IndexLayout>
    )
  }
}

export default Portfolio
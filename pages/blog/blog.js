import React from 'react'
import Link from 'next/link'
import Head from 'next/head'
import IndexLayout from '../../components/IndexLayout'

import {
  Button,
  Container,
  Grid,
  Icon,
  Image,
  Label,
  Step,
  Breadcrumb
} from 'semantic-ui-react'

import fetch from 'isomorphic-unfetch'
import ReactMarkdown from 'react-markdown'

import SyntaxHighlighter from 'react-syntax-highlighter'
import { monokai } from 'react-syntax-highlighter/dist/styles/hljs'
import Disqus from '../../components/Disqus'

const inlineCode = props => <strong>{props.value}</strong>
const code = props => <SyntaxHighlighter language='javascript' style={monokai}>{props.value}</SyntaxHighlighter>
const tableRow = props => <tr className="foo">{props.children}</tr>

import getConfig from 'next/config'
const {serverRuntimeConfig, publicRuntimeConfig} = getConfig()

class Index extends React.Component {
  static async getInitialProps (context, apolloClient) {
    const slug = context.query.params.blog

		const blogs = await fetch(`${publicRuntimeConfig.assetPrefix}/output/blog.json`)
			.then((response) => {
        return response.json()
      })
			.then((data) => {
        let blogs = []
        Object.keys(data.fileMap).map(function (value, index) {
          blogs.push(data.fileMap[value])
        })

        return blogs
		  })
			.catch((error) => {
        console.error(error.toString())
      })
    
    const index = blogs.find(function (blog) {
      return blog.slug === slug
    })

    let dir = index.dir.slice(7) // trabur/
		const blog = await fetch(`${publicRuntimeConfig.assetPrefix}/${dir}/${index.base}`)
      .then((response) => {
        return response.json()
      })
      .then((data) => {
        return data
      })
      .catch((error) => {
        console.error(error.toString())
      })

    return { blog }
  }

  constructor(props) {
    super(props);
    
    this.state = {
      blog: this.props.blog,
      url: `https://trabur.metaheap.io/blog/${this.props.blog.slug}`
    }
  }

  render () {
    return (
      <IndexLayout 
        title={`${this.state.blog.title} - TRAVIS BURANDT`}
        meta={`${this.state.blog.description}`}
        activeItem='blog'
      >
        <Container
          style={{
            background: 'rgba(0, 0, 0, 0.8)',
            border: '2px solid #fff',
            padding: '1em',
            marginBottom: '1em',
            color: '#fff'
          }}
        >
          <Breadcrumb size='massive'>
            <Link href={'/'}>
              <Breadcrumb.Section link style={{ color: '#f7bf4d' }}>TRABUR</Breadcrumb.Section>
            </Link>
            <Breadcrumb.Divider icon='right chevron' style={{ color: '#ccc' }} />
            <Link href={'/blog'}>
              <Breadcrumb.Section link style={{ color: '#f7bf4d' }}>Blog</Breadcrumb.Section>
            </Link>
            <Breadcrumb.Divider icon='right chevron' style={{ color: '#ccc' }} />
            <Breadcrumb.Section>{this.state.blog.title}</Breadcrumb.Section>
          </Breadcrumb>
        </Container>
        <Container style={{ marginBottom: '1em' }}>
          <Image  src={`${publicRuntimeConfig.assetPrefix}${this.state.blog.cover}`} />
        </Container>
        <Container 
            style={{
              background: '#333',
              border: '2px solid #fff',
              padding: '1em',
              color: '#fff',
              marginBottom: '1em'
            }}
        >
          <Container text>
            <ReactMarkdown 
              source={this.state.blog.bodyContent} 
              renderers={{code, inlineCode, tableRow}} 
              escapeHtml={false}
            />
          </Container>
        </Container>
        <Container 
            style={{
              background: 'rgba(255,255,255, 0.9)',
              padding: '1em',
            }}
        >
          <Container text>
            <Disqus title={this.state.blog.title} url={this.state.url} identifier={this.state.blog.slug} />
          </Container>
        </Container>
      </IndexLayout>
    )
  }
}

export default Index
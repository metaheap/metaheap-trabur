module.exports = {
  apps: [
    {
      name: "metaheap-trabur",
      script: "./index.js",
      instances: "max",
      env: {
        "NODE_ENV": "development",
        "ASSET_PREFIX": 'https://metaheap.sfo2.cdn.digitaloceanspaces.com/trabur'
      },
      env_production: {
        "NODE_ENV": "production",
        "ASSET_PREFIX": 'https://metaheap.sfo2.cdn.digitaloceanspaces.com/trabur'
      },
      exec_mode: "cluster"
    }
  ]
}

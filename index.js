const express = require('express')
const next = require('next')

const port = parseInt(process.env.PORT, 10) || 7548
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()
var bodyParser = require('body-parser')
const Prometheus = require('prom-client')

const httpRequestDurationMicroseconds = new Prometheus.Histogram({
  name: 'http_request_duration_ms',
  help: 'Duration of HTTP requests in ms',
  labelNames: ['method', 'route', 'code'],
  buckets: [0.10, 5, 15, 50, 100, 200, 300, 400, 500]  // buckets for response time from 0.1ms to 500ms
})

Prometheus.collectDefaultMetrics()

app
  .prepare()
  .then(() => {
    const server = express()

    // Runs before each requests
    server.use((req, res, next) => {
      res.locals.startEpoch = Date.now()
      next()
    })

    // parse application/x-www-form-urlencoded
    server.use(bodyParser.urlencoded({ extended: false }))

    // parse application/json
    server.use(bodyParser.json())

    server.get('/metrics', (req, res) => {
      res.set('Content-Type', Prometheus.register.contentType)
      res.end(Prometheus.register.metrics())
    })

    server.get('/blog/:blog', (req, res) => {
      return app.render(req, res, '/blog/blog', { query: req.query, params: req.params })
    })

    server.get('*', (req, res) => {
      return handle(req, res)
    })

    // Runs after each requests
    server.use((req, res, next) => {
      const responseTimeInMs = Date.now() - res.locals.startEpoch

      httpRequestDurationMicroseconds
        .labels(req.method, req.path, res.statusCode)
        .observe(responseTimeInMs)

      next()
    })

    // if (dev) {
      server.listen(port, (err) => {
        if (err) throw err
        console.log(`> Ready on http://127.0.0.1:${port}`)
      })
    // } else {
    //   require('greenlock-express').create({
    //     // Let's Encrypt v2 is ACME draft 11
    //     version: 'draft-11',
  
    //     server: 'https://acme-v02.api.letsencrypt.org/directory',
    //     // Note: If at first you don't succeed, switch to staging to debug
    //     // https://acme-staging-v02.api.letsencrypt.org/directory
  
    //     // You MUST change this to a valid email address
    //     email: 'travis.burandt@gmail.com',
  
    //     // You MUST NOT build clients that accept the ToS without asking the user
    //     agreeTos: true,
  
    //     // You MUST change these to valid domains
    //     // NOTE: all domains will validated and listed on the certificate
    //     approveDomains: [ 'hvac-buddy.com', 'www.hvac-buddy.com' ],
  
    //     // You MUST have access to write to directory where certs are saved
    //     // ex: /home/foouser/acme/etc
    //     configDir: require('path').join(require('os').homedir(), 'acme', 'etc'),
  
    //     app: server,
    //     // app: require('express')().use('/', function (req, res) {
    //     //   res.setHeader('Content-Type', 'text/html; charset=utf-8')
    //     //   res.end('Hello, World!\n\n💚 🔒.js');
    //     // }),
  
    //     // Join the community to get notified of important updates and help me make greenlock better
    //     communityMember: false,
  
    //     // Contribute telemetry data to the project
    //     telemetry: false,
  
    //     //debug: true
  
    //   }).listen(80, 443)
    // }
  })

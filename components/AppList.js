import Link from 'next/link'
import Head from 'next/head'

import {
  Button,
  Container
} from 'semantic-ui-react'

class AppList extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      // active: this.props.active
    }
  }

  render() {
    return (
      <Container style={{ textAlign: 'center', margin: '0 0 5em 0' }}>
        <Button size='massive' as='a' href='//trabur.metaheap.io' inverted style={{ margin: '0.5em' }}>TRABUR</Button>
        <Button size='massive' as='a' href='//plex.metaheap.io' inverted style={{ margin: '0.5em' }}>PLEX</Button>
      </Container>
    )
  }
}

export default AppList
import Link from 'next/link'
import Head from 'next/head'

import {
  Container,
  Button
} from 'semantic-ui-react'

class TagCloud extends React.Component {
  constructor(props) {
    super(props)

    let tags = []
    this.props.array.map(function (value, index) {
      value.tags.map(function (tag, index) {
        let checkTag = function (element) {
          return element.name === tag 
        }
        let checkIndex = tags.findIndex(checkTag)
        // console.log('checkIndex', checkIndex)

        // check to see if tag is already in the tags array
        if (checkIndex !== -1) {
          // if it is...
          // increment count + 1
          tags[checkIndex] = {
            name: tags[checkIndex].name,
            count: tags[checkIndex].count + 1
          }
        } else {
          // if it is not...
          // create new
          tags.push({
            name: tag,
            count: 1
          })
        }
      })
    })

    tags = tags.sort(function(a, b) {
      return a.count - b.count
    }).reverse()

    this.state = {
      array: this.props.array,
      type: this.props.type, // ['blog', 'portfolio']
      tags,
      // tags: [
      //   {
      //     name: 'first',
      //     count: 10
      //   }
      // ],
      search: this.props.search
    }
  }

  render() {
    let that = this
    return (
      <Container style={{ 
        marginBottom: '1em', 
        textAlign: 'center',
        padding: '1em',
        color: '#fff',
      }}>
        {
          this.state.tags.map(function (tag, index) {
            return (
              <Button
                key={index}
                content={tag.name}
                label={{ basic: true, content: tag.count }}
                labelPosition='right'
                color={ that.state.search == tag.name ? 'green' : null }
                onClick={() => {
                  window.location = `/${that.state.type}?search=${tag.name}`
                }}
                style={{
                  margin: '0.2em'
                }}
              />
            )
          })
        }
      </Container>
    )
  }
}

export default TagCloud
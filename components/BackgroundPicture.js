import Link from 'next/link'
import Head from 'next/head'

import {
  Image
} from 'semantic-ui-react'

import getConfig from 'next/config'
const {serverRuntimeConfig, publicRuntimeConfig} = getConfig()

class BackgroundPicture extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
    }
  }

  render() {
    return (
      <React.Fragment>
        <div
          className='mobile only'
          style={{
            height: '200px',
            width: '100%',
            background: `url(${publicRuntimeConfig.assetPrefix}/static/images/background-picture.jpg)`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center'
          }}
        >
        </div>
        <div
          className='tablet only'
          style={{
            height: '400px',
            width: '100%',
            background: `url(${publicRuntimeConfig.assetPrefix}/static/images/background-picture.jpg)`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center'
          }}
        >
        </div>
        <div
          className='computer only'
          style={{
            height: '500px',
            width: '100%',
            background: `url(${publicRuntimeConfig.assetPrefix}/static/images/background-picture.jpg)`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center'
          }}
        >
        </div>
        <div
          className='large screen only'
          style={{
            height: '600px',
            width: '100%',
            background: `url(${publicRuntimeConfig.assetPrefix}/static/images/background-picture.jpg)`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center'
          }}
        >
        </div>
        <div
          className='widescreen only'
          style={{
            height: '700px',
            width: '100%',
            background: `url(${publicRuntimeConfig.assetPrefix}/static/images/background-picture.jpg)`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center'
          }}
        >
        </div>
      </React.Fragment>
    )
  }
}

export default BackgroundPicture
import Link from 'next/link'
import Head from 'next/head'

import {
  Icon,
  Input, 
  Menu
} from 'semantic-ui-react'

import Router from 'next/router'

class Logo extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      activeItem: this.props.activeItem,
      searchBlog: this.props.search,
      searchPortfolio: this.props.search,
    }
  }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  handleSearchBlog = (event, search) => {
    this.setState({
      searchBlog: search.value
    })
  }

  handleSearchPortfolio = (event, search) => {
    this.setState({
      searchPortfolio: search.value
    })
  }

  render() {
    let activeItem = this.state.activeItem
    return (
      <Menu secondary>
        <Link href='/'>
          <Menu.Item
            name='Home' 
            active={activeItem === 'home'} 
          />
        </Link>
        <Link href='/blog'>
          <Menu.Item 
            name='Blog'
            active={activeItem === 'blog'}
          />
        </Link>
        <Link href='/portfolio'>
          <Menu.Item 
            name='Portfolio'
            active={activeItem === 'portfolio'}
          />
        </Link>
        <Menu.Menu position='right' className='mobile hidden'>
          <Menu.Item
            as='a'
            href='https://gitlab.com/metaheap' 
            target='_blank'
            name='Gitlab'
          >
            <Icon name='gitlab' />
          </Menu.Item>
          <Menu.Item
            as='a'
            href='https://github.com/metaheap'
            target='_blank'
            name='Github'
          >
            <Icon name='github' />
          </Menu.Item>
          {
            this.state.activeItem === 'home' || this.state.activeItem === 'blog'
              ?
                <Menu.Item>
                  <Input 
                    icon='search' 
                    placeholder='Search...' 
                    value={this.state.searchBlog} 
                    onChange={this.handleSearchBlog}
                    onKeyPress={(e) => {
                      if (e.key === 'Enter') {
                        window.location = `/blog?search=${this.state.searchBlog}`
                      }
                    }} />
                </Menu.Item>  
              : null
          }
          {
            this.state.activeItem === 'portfolio'
              ?
                <Menu.Item>
                  <Input 
                    icon='search' 
                    placeholder='Search...' 
                    value={this.state.searchPortfolio} 
                    onChange={this.handleSearchPortfolio}
                    onKeyPress={(e) => {
                      if (e.key === 'Enter') {
                        window.location = `/portfolio?search=${this.state.searchPortfolio}`
                      }
                    }} />
                </Menu.Item>  
              : null
          }
        </Menu.Menu>
      </Menu>
    )
  }
}

export default Logo
import Link from 'next/link'
import Head from 'next/head'

import {
  Icon,
  Input, 
  Menu,
  Grid,
  Card,
  Button,
  Image
} from 'semantic-ui-react'

import getConfig from 'next/config'
const {serverRuntimeConfig, publicRuntimeConfig} = getConfig()

class Showcase extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      projects: this.props.projects,
      mobile: true,
      web: true,
      nodejs: true,
      type: this.props.type
    }
  }

  // componentWillReceiveProps = (nextProps) => {
  //   if (this.state.type !== nextProps.type) {
  //     console.log('showcase cwrp type', nextProps.type)
  //     this.handleType(nextProps.type)
  //     this.setState({
  //       type: nextProps.type
  //     })
  //   }
  // }

  componentDidMount() {
    this.handleType()
  }

  handleType = (type) => {
    type = type || this.state.type
    if (type === 'mobile') {
      this.setState({
        mobile: true,
        web: false,
        nodejs: false
      })
    } else if (type === 'web') {
      this.setState({
        mobile: false,
        web: true,
        nodejs: false
      })
    } else if (type === 'nodejs') {
      this.setState({
        mobile: false,
        web: false,
        nodejs: true
      })
    }
  }
  
  handleMobile = () => {
    // if all three are lit keep current on and turn off the others
    if (this.state.mobile === true && this.state.web === true && this.state.nodejs === true) {
      this.setState({ 
        web: false,
        nodejs: false
      })
    } else {
      this.setState(prevState => ({ mobile: !prevState.mobile }))
    }
  }

  handleWeb = () => {
    if (this.state.mobile === true && this.state.web === true && this.state.nodejs === true) {
      this.setState({ 
        mobile: false,
        nodejs: false
      })
    } else {
      this.setState(prevState => ({ web: !prevState.web }))
    }
  }

  handleNodejs = () => {
    if (this.state.mobile === true && this.state.web === true && this.state.nodejs === true) {
      this.setState({ 
        mobile: false,
        web: false
      })
    } else {
      this.setState(prevState => ({ nodejs: !prevState.nodejs }))
    }
  }

  render() {
    let that = this
    return (
      <Grid columns={3} stackable>
        <Grid.Row>
          <Grid.Column>
            <Card fluid>
              <Image src={`${publicRuntimeConfig.assetPrefix}/static/portfolio/mobile.png`} />
              <Card.Content>
                <Button 
                  size='massive' 
                  fluid 
                  style={ this.state.mobile === true ? { backgroundColor: '#33a64b' } : {} }
                  onClick={this.handleMobile}
                >
                  mobile
                </Button>
              </Card.Content>
            </Card>
          </Grid.Column>
          <Grid.Column>
            <Card fluid>
              <Image src={`${publicRuntimeConfig.assetPrefix}/static/portfolio/web.png`} />
              <Card.Content>
                <Button 
                  size='massive' 
                  fluid 
                  style={ this.state.web === true ? { backgroundColor: '#33a64b' } : {} }
                  onClick={this.handleWeb}
                >
                  web
                </Button>
              </Card.Content>
            </Card>
          </Grid.Column>
          <Grid.Column>
            <Card fluid>
              <Image src={`${publicRuntimeConfig.assetPrefix}/static/portfolio/nodejs.png`} />
              <Card.Content>
                <Button 
                  size='massive' 
                  fluid 
                  style={ this.state.nodejs === true ? { backgroundColor: '#33a64b' } : {} }
                  onClick={this.handleNodejs}
                >
                  node.js
                </Button>
              </Card.Content>
            </Card>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16}>
            <Card.Group centered>
              {
                this.state.projects
                  .filter(function (project) {
                    if (that.state.mobile && project.type === 'mobile') {
                      return true
                    }
                    if (that.state.web && project.type === 'web') {
                      return true
                    }
                    if (that.state.nodejs && project.type === 'nodejs') {
                      return true
                    }
                    return false
                  })
                  .map(function (value, index) {
                    return (
                      <Link key={`${value.dir}/${value.base}`} href={`/portfolio/${value.slug}`}>
                        <Card>
                          <Image src={`${publicRuntimeConfig.assetPrefix}${value.coverPreview}`} />
                          <Card.Content>
                            <Card.Header>{value.title}</Card.Header>
                            <Card.Meta>{value.type}</Card.Meta>
                            <Card.Description>{value.description}</Card.Description>
                          </Card.Content>
                        </Card>
                      </Link>
                    )
                  })
              }
            </Card.Group>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}

export default Showcase
import Link from 'next/link'
import Head from 'next/head'

import {
  Image
} from 'semantic-ui-react'

import getConfig from 'next/config'
const {serverRuntimeConfig, publicRuntimeConfig} = getConfig()

class ProfilePicture extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
    }
  }

  render() {
    return (
      <div 
        style={{
          position: 'absolute',
          bottom: 0,
          width: 'calc(100% - 1em)',
          right: 0
        }}
      >
        <div
          style={{
            height: '180px',
            width: '100%',
            background: `url(${publicRuntimeConfig.assetPrefix}/static/images/profile-picture.jpg)`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center'
          }}
        >
        </div>
      </div>
    )
  }
}

export default ProfilePicture
import Link from 'next/link'
import Head from 'next/head'

import { initGA, logPageView } from '../utils/analytics'

import {
  Button,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  Statistic,
  Segment,
  Card,
  Visibility,
  Sidebar,
} from 'semantic-ui-react'

import Particles from 'react-particles-js'
import Menu from './Menu'
import ProfilePicture from './ProfilePicture'
import BackgroundPicture from './BackgroundPicture'
import Footer from './Footer'
import ReactDOM from 'react-dom'

import getConfig from 'next/config'
const {serverRuntimeConfig, publicRuntimeConfig} = getConfig()

class IndexLayout extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      children: this.props.children,
      title: this.props.title || 'This is the default title', 
      meta: this.props.meta,
      activeItem: this.props.activeItem,
      search: this.props.search
    }
  }

  componentDidMount() {
    const domNode = ReactDOM.findDOMNode(this.refs.scrollTo)
    domNode.scrollIntoView()

    if (!window.GA_INITIALIZED) {
      initGA()
      window.GA_INITIALIZED = true
    }
    logPageView()
  }

  render() {
    return (
      <div>
        <Head>
          <title>{this.state.title}</title>
          <meta name='description' content={this.state.meta} />
          <meta charSet='utf-8' />
          <meta name='viewport' content='width=500, initial-scale=1.0, width=device-width' />
          <link rel='shortcut icon' type='image/x-icon' href={`${publicRuntimeConfig.assetPrefix}/static/favicon.ico`} />
          <link rel="apple-touch-icon" sizes="57x57" href={`${publicRuntimeConfig.assetPrefix}/static/apple-icon-57x57.png`} />
          <link rel="apple-touch-icon" sizes="60x60" href={`${publicRuntimeConfig.assetPrefix}/static/apple-icon-60x60.png`} />
          <link rel="apple-touch-icon" sizes="72x72" href={`${publicRuntimeConfig.assetPrefix}/static/apple-icon-72x72.png`} />
          <link rel="apple-touch-icon" sizes="76x76" href={`${publicRuntimeConfig.assetPrefix}/static/apple-icon-76x76.png`} />
          <link rel="apple-touch-icon" sizes="114x114" href={`${publicRuntimeConfig.assetPrefix}/static/apple-icon-114x114.png`} />
          <link rel="apple-touch-icon" sizes="120x120" href={`${publicRuntimeConfig.assetPrefix}/static/apple-icon-120x120.png`} />
          <link rel="apple-touch-icon" sizes="144x144" href={`${publicRuntimeConfig.assetPrefix}/static/apple-icon-144x144.png`} />
          <link rel="apple-touch-icon" sizes="152x152" href={`${publicRuntimeConfig.assetPrefix}/static/apple-icon-152x152.png`} />
          <link rel="apple-touch-icon" sizes="180x180" href={`${publicRuntimeConfig.assetPrefix}/static/apple-icon-180x180.png`} />
          <link rel="icon" type="image/png" sizes="192x192"  href={`${publicRuntimeConfig.assetPrefix}/static/android-icon-192x192.png`} />
          <link rel="icon" type="image/png" sizes="32x32" href={`${publicRuntimeConfig.assetPrefix}/static/favicon-32x32.png`} />
          <link rel="icon" type="image/png" sizes="96x96" href={`${publicRuntimeConfig.assetPrefix}/static/favicon-96x96.png`} />
          <link rel="icon" type="image/png" sizes="16x16" href={`${publicRuntimeConfig.assetPrefix}/static/favicon-16x16.png`} />
          <link rel="manifest" href={`${publicRuntimeConfig.assetPrefix}/static/manifest.json`} />
          <meta name="msapplication-TileColor" content="#ffffff" />
          <meta name="msapplication-TileImage" content={`${publicRuntimeConfig.assetPrefix}/static/ms-icon-144x144.png`} />
          <meta name="theme-color" content="#ffffff" />
          <link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.12/semantic.min.css' />
          <link rel="stylesheet" type="text/css" charSet="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
          <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
          <link rel='stylesheet' href={`${publicRuntimeConfig.assetPrefix}/static/index.css`} />
        </Head>
        <Particles
          width="100%"
          height="100%"
          style={{ 
            position: 'absolute',
            top: '0',
            right: '0',
            bottom: '0',
            left: '0',
            background: '#111',
            zIndex: 0
          }}
        />
        <div style={{
            position: 'absolute',
            top: '0',
            right: '0',
            bottom: '0',
            left: '0',
            zIndex: 1,
            overflowY: 'auto'
        }}>
          <Container
            style={{
              background: '#fff',
              padding: '0.5em',
              marginBottom: '1em'
            }}>
            <Grid>
              <Grid.Row style={{ padding: '0 0.5em' }}>
                <BackgroundPicture />
              </Grid.Row>
              <Grid.Row style={{ marginTop: '-11em' }}>
                <Grid.Column width={3} className='mobile hidden'>
                </Grid.Column>
                <Grid.Column width={13}>
                  <Link href='/'>
                    <h1 ref='scrollTo' style={{ margin: 0, paddingTop: '1.1em', color: '#fff', textShadow: '2px 2px #000', cursor: 'pointer' }}>Travis Burandt</h1>
                  </Link>
                  <h3 style={{ margin: 0, color: '#fff', textShadow: '2px 2px #000' }}>Full-Stack <strong>JavaScript</strong> Developer</h3>
                  <h3 style={{ margin: 0, color: '#fff', textShadow: '2px 1px #000' }}>
                    <a href='/portfolio?type=mobile'>
                      <span style={{ color: '#fff', cursor: 'pointer', textDecorationLine: 'underline' }}>mobile</span>
                    </a> | <a href='/portfolio?type=web'>
                      <span style={{ color: '#fff', cursor: 'pointer', textDecorationLine: 'underline' }}>web</span>
                    </a> | <a href='/portfolio?type=nodejs'>
                      <span style={{ color: '#fff', cursor: 'pointer', textDecorationLine: 'underline' }}>node.js</span>
                    </a>
                  </h3>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row style={{ padding: '0.5em 0 1em 0' }} className='mobile hidden'>
                <Grid.Column width={3}>
                  <ProfilePicture />
                </Grid.Column>
                <Grid.Column width={13}>
                  <Menu activeItem={this.state.activeItem} search={this.state.search} />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row style={{ padding: '0.5em 0 1em 0' }} className='mobile only'>
                <Grid.Column width={16}>
                  <Menu activeItem={this.state.activeItem} search={this.state.search} />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Container>
          {this.state.children}
          <Footer />
        </div>
      </div>
    )
  }
}

export default IndexLayout
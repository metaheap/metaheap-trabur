import React from 'react'

import Disqus from 'disqus-react'

class Index extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loaded: false,
      title: this.props.title,
      identifier: this.props.identifier,
      url: this.props.url
    }
  }

  componentDidMount() {
    this.setState({
      loaded: true
    })
  }

  render() {
    if (this.state.loaded) {
      const disqusShortname = 'trabur'
      const disqusConfig = {
          url: this.state.url,
          identifier: this.state.identifier,
          title: this.state.title,
      }
  
      return (
        <Disqus.DiscussionEmbed shortname={disqusShortname} config={disqusConfig} />
      )
    } else {
      return null
    }
  }
}
    
export default Index
import Link from 'next/link'
import Head from 'next/head'

import {
  Icon
} from 'semantic-ui-react'

class Footer extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
    }
  }

  render() {
    return (
      <a href='//www.metaheap.io'>
        <div style={{ margin: '6em auto 10em', textAlign: 'center' }}>
          <Icon name='circle' style={{ color: '#1CA4FC', fontSize: '8em', lineHeight: '110px' }} />
          <Icon name='play' style={{ color: '#33A64B', fontSize: '8em', lineHeight: '110px' }} />
          <Icon name='square' style={{ color: '#F7BF4D', fontSize: '8em', lineHeight: '110px'}} />
        </div>
      </a>
    )
  }
}

export default Footer